#include "pico/stdlib.h"

int main()
{
    stdio_init_all();

    for (int x = 2; x < 10; x++)
    {
        gpio_init(x);
        gpio_set_dir(x, GPIO_OUT);
    }

    while (true)
    {
        for (int i = 2; i < 10; i++)
        {
            gpio_put(i, 1);
            sleep_ms(100);
        }

        for (int y = 2; y < 10; y++)
        {
            gpio_put(y, 0);
            sleep_ms(100);
        }
    }
}
