#include <stdio.h>
#include "pico/stdlib.h"
#include "pico/multicore.h"
#include "pico/util/queue.h"

queue_t send_queue;
queue_t recieve_queue;

void core1_main()
{
    while(true)
    {
        int32_t entry;
        queue_remove_blocking(&send_queue, &entry);
        int32_t result = entry*2;
        queue_add_blocking(&recieve_queue, &result);
    }

}



int main()
{
    int32_t i = 1;
    int32_t x;
    stdio_init_all();

    printf("Hello, using queues to send and recieve!\n");

    queue_init(&send_queue, sizeof(int32_t), 1);
    queue_init(&recieve_queue, sizeof(int32_t), 1);

    multicore_launch_core1(core1_main);

    queue_add_blocking(&send_queue, &i);
    queue_remove_blocking(&recieve_queue, &x);

    printf("Response from core1 is %d\n", x);
}
