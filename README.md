Cointains small projects with the purpose to get to know the RP2040
micro controller from Raspberry.

##### Prerequisites Hardware:
* Raspberry Pico RP2040
* Raspberry Pi Debug Probe
* Maker Pi Pico

##### Prerequisites Software:
* ARM GCC Compiler
* Raspberry Pi Pico SDK
* OpenOCD
* gdb-multiarch
* minicom


##### How to build, deploy and debug:

```
$ cd project
$ mkdir build
$ cd build
$ cmake ..
$ make -j4
$ sudo openocd -f interface/cmsis-dap.cfg -f target/rp2040.cfg -c "adapter speed 5000" -c "program file.elf verify reset exit"
$ minicom -b 115200 -o -D /dev/ttyACM0
```

Exchange "project" with the real project name and in the command you write "sudo" change
"file.elf" to the real filename.
