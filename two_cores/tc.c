#include <stdio.h>
#include "pico/stdlib.h"
#include "pico/multicore.h"

void core1_main()
{
    int i = 0;

    while(true)
    {
        printf("core %d : %d\n", get_core_num(), i++);
        sleep_ms(100);
    }
}


int main()
{
    stdio_init_all();
    sleep_ms(1000);
    int i = 0;

    multicore_launch_core1(core1_main);

    while(true)
    {
        printf("core %d : %d\n", get_core_num(), i++);
        sleep_ms(100);
    }
}
