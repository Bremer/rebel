#include <stdio.h>
#include "pico/stdlib.h"

#define PIN_IN 20
#define PIN_OUT 2

int main()
{
    stdio_init_all();

    gpio_init(PIN_OUT);
    gpio_set_dir(PIN_OUT, GPIO_OUT);

    gpio_init(PIN_IN);
    gpio_set_dir(PIN_IN, GPIO_IN);

    while (true)
    {
        while(!gpio_get(PIN_IN))
        {
            gpio_put(PIN_OUT, 1);
            printf("Button on pin 20 pushed.\n");
        }
        gpio_put(PIN_OUT, 0);
    }

}
