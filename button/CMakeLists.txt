cmake_minimum_required(VERSION 3.13)

set(CMAKE_CXX_STANDARD 23)

include(pico_sdk_import.cmake)

project(button)
pico_sdk_init()

add_executable(button
	button.c
	)

target_link_libraries(button pico_stdlib)

pico_add_extra_outputs(button)
