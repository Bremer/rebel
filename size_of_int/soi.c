#include <stdio.h>
#include "pico/stdlib.h"

int main()
{
    stdio_init_all();

    while (true)
    {
        printf("Size of integer: %d\n", sizeof(int));
        sleep_ms(1000);
    }
}
