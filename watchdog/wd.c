#include <stdio.h>
#include <stdlib.h>
#include "pico/stdlib.h"
#include "hardware/watchdog.h"

struct node {
    int score;
    struct node *next;
};

void insert(int score, struct node **head);

int main()
{
    stdio_init_all();

    if (watchdog_caused_reboot())
    {
        printf("Rebooted by Watchdog!\n");
        sleep_ms(10000);
    }
    else
    {
        printf("Clean boot\n");
    }

    watchdog_enable(100, 0);

    struct node *head = NULL;
    int i = 0;

    while (true)
    {
        insert(i, &head);
        printf("%d number of structs\n", i++);
        watchdog_update();
    }
}

void insert(int score, struct node **head)
{
    struct node *link = malloc(sizeof(struct node));
    link->score = score;
    link->next = *head;
    *head = link;
}
