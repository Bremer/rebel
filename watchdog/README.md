Building a linked list which will consume all memory.
The watchdog will reboot the RP2040 when it runs out of memory.

```
16142 number of structs
16143 number of structs
16144 number of structs
16145 number of structs
16146 number of structs
16147 number of structs
16148 number of structs
16149 number of structs

*** PANIC ***

Out of memory

Rebooted by Watchdog!

```

