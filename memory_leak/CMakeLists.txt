cmake_minimum_required(VERSION 3.13)

set(CMAKE_CXX_STANDARD 23)

include(pico_sdk_import.cmake)

project(ml)
pico_sdk_init()

add_executable(ml
	ml.c
	)

target_link_libraries(ml pico_stdlib)

pico_add_extra_outputs(ml)
