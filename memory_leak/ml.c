#include <stdio.h>
#include <stdlib.h>
#include "pico/stdlib.h"

struct node {
    int score;
    struct node *next;
};

void insert(int score, struct node **head);

int main()
{
    stdio_init_all();
    struct node *head = NULL;
    int i = 0;

    while (true)
    {
        insert(i, &head);
        printf("%d number of structs\n", i);
        i++;
    }
}

void insert(int score, struct node **head)
{
    struct node *link = malloc(sizeof(struct node));
    link->score = score;
    link->next = *head;
    *head = link;
}
